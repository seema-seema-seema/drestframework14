from rest_framework import serializers
from .models import Category, Post
from django.contrib.auth.models import User


class UserSerializer1(serializers.ModelSerializer):
    # posts = serializers.PrimaryKeyRelatedField(
    # many=True, queryset=Post.objects.all())

    class Meta:
        model = User
        # fields = ['id', 'username', 'posts']
        fields = ['id', 'username']


class PostSerializer1(serializers.ModelSerializer):

   # owner = serializers.StringRelatedField(many=False)
    owner = UserSerializer1(many=False, read_only=True)

    # NOTE : enu comnt krna optonal h ,TA KRKE HIDE KITA BECUZ EH 2 VAR SHOW HO RHA SANU ,CATEGORY VALE TABLE CH V H TE ETHE V
   # category = serializers.SlugRelatedField(
    # many=False, read_only=True, slug_field='title')

    class Meta:
        model = Post
        # fields = '__all__'

        # fields = ['id', 'title', 'body', 'owner','category]
        fields = ['id', 'title', 'body', 'owner']


class CategorySerializer(serializers.ModelSerializer):
    posts = PostSerializer1(
        many=True, read_only=True, source='post_set')  # NOTE :SOURCE='POST_SET' TA USE KITA beucz models.py ch pehla category vala a te badh ch post vala islyi sanu oda reverse dasna pena

    class Meta:
        model = Category
        fields = ['id', 'title', 'created_at', 'posts']


class PostSerializer(serializers.ModelSerializer):

    owner = UserSerializer1(many=False, read_only=True)

    category = serializers.SlugRelatedField(
        many=False, read_only=True, slug_field='title')

    url = serializers.HyperlinkedIdentityField(
        view_name="single-post", read_only=True)

    class Meta:
        model = Post
        # fields = '__all__'

        fields = ['id', 'url', 'title', 'body', 'owner', 'category']


class UserSerializer(serializers.ModelSerializer):
    posts = serializers.PrimaryKeyRelatedField(
        many=True, queryset=Post.objects.all())

    class Meta:
        model = User
        fields = ['id', 'username', 'posts']
