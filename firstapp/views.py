from .models import Post, Category
from .serializers import PostSerializer, CategorySerializer, UserSerializer
from rest_framework import generics
from django.contrib.auth.models import User
from rest_framework import permissions, viewsets
from firstapp.permissions import IsOwnerOrReadOnly, IsOwnerOrIsAdminOrReadOnly
from rest_framework.reverse import reverse
from rest_framework.response import Response
from rest_framework.views import APIView


# Create your views here.


class PostViewSet(viewsets.ModelViewSet):
    # NOTE : CASE 1-----> EDE CH USER HI APNI POST NU EDIT KR SAKDA A BES ,HOR KOI NI EVEN K ADMIN V EDIT/UPDATE NI KR SAKDA +permision.py file ch code kita

    # permission_classes = [
    #  permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]

    # NOTE : CASE 2-------> EDE CH USER EDIT KR SAKDA + ADMIN V NAL KISE V USER DE POST NU EDIT KR SAKDA H + permisions.py file ch code kita edA
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly, IsOwnerOrIsAdminOrReadOnly]

    queryset = Post.objects.all()
    serializer_class = PostSerializer

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


'''class PostList(generics.ListCreateAPIView):

    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    queryset = Post.objects.all()
    serializer_class = PostSerializer

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)'''


'''class PostDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [permissions.IsAuthenticatedOrReadOnly,
                          IsOwnerOrReadOnly]

    queryset = Post.objects.all()
    serializer_class = PostSerializer '''


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


'''class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer'''


class CategoryViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


'''

class CategoryList(generics.ListAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class CategoryDetail(generics.RetrieveAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer'''


# class CategoryCreate(generics.CreateAPIView): #sare category add kr sakde (computers,electrionics)
# sirf admin hi actegory nu add kr sakda (like electonics ,computers etc)
class CategoryCreate(generics.ListCreateAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = [permissions.IsAdminUser]


class ApiRoot(APIView):
    def get(self, request, format=None):
        return Response({
            # eh krn toh pehla asi url.py ch dena +category detal vle uper 2 functions jada bnaye ede ch
            # users ,posts,categiories vgera yrls,py ch name= vale same anme use kite
            'users': reverse('users', request=request, format=format),
            'posts': reverse('posts', request=request, format=format),
            'categories': reverse('categories', request=request, format=format),
            'categoryAdd': reverse('create_category', request=request, format=format),

        })
